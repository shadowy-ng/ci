#!/usr/bin/env bash
cd $1
../../node_modules/.bin/next-ver --go
cd ../../
PACKAGE_VERSION=$(sed -nE 's/^\s*"version": "(.*?)",$/\1/p' "$1/package.json")
echo $PACKAGE_VERSION
git add $1/package.json
git commit -m "$PACKAGE_VERSION"
git tag -a "v$PACKAGE_VERSION" -m "$PACKAGE_VERSION"
git push
git push --tags


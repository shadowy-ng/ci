const
  fs = require('fs'),
  https = require('https'),
  colorError = '\x1b[31m',
  colorGood = '\x1b[32m',
  colorCommon = '\x1b[37m',
  fileResult = './.scannerwork/report-task.txt',
  taskStatusForWait = ['IN_PROGRESS', 'PENDING'],
  taskStatusForError = ['CANCELED', 'FAILED'];

function parseIniFile(data) {
  const lines = data.replace(/\r/g, '').split('\n');
  return lines.reduce((obj, line) => {
    const pos = line.indexOf('=');
    if (pos < 1) {
      return obj;
    }
    obj[line.substring(0, pos)] = line.substring(pos + 1);
    return obj;
  }, {});
}

function getColor(status) {
  let color = colorGood;
  if (status === 'ERROR') {
    color = colorError;
  }
  return color;
}

function httpGet(url) {
  return new Promise((resolve, reject) => {
    https.get(url, (resp) => {
      let data = '';

      resp.on('data', (chunk) => data += chunk);
      resp.on('end', () => {
        resolve(JSON.parse(data))
      });
    }).on("error", err => reject(err));
  });
}

function getAnalysisId(url) {
  return new Promise((resolve, reject) => {
    function get() {
      httpGet(url).then(result => {
        const status = `TASK STATUS: ${result.task.status}`;
        if (taskStatusForWait.indexOf(result.task.status) >= 0) {
          console.log(colorCommon, status);
          return setTimeout(get, 10000);
        }
        if (taskStatusForError.indexOf(result.task.status) >= 0) {
          console.log(colorError, status);
          return reject();
        }
        console.log(colorGood, status);
        console.log(colorGood, `TASK analysisId: ${result.task.analysisId}`);
        resolve(result.task.analysisId)
      })
    };

    get();
  });
}

if (!fs.existsSync(fileResult)) {
  return process.exit(-404);
}
const data = parseIniFile(fs.readFileSync(fileResult).toString());

getAnalysisId(data.ceTaskUrl).then(result =>
  httpGet(data.serverUrl + '/api/qualitygates/project_status?analysisId=' + result).then(result => {
    console.log(getColor(result.projectStatus.status),
      '-----------------------------------------------------------------------------');
    console.log(getColor(result.projectStatus.status), 'Status: ' + result.projectStatus.status);
    console.log(getColor(result.projectStatus.status),
      '-----------------------------------------------------------------------------');
    result.projectStatus.conditions.forEach(condition => {
      console.log(getColor(condition.status), `${condition.metricKey} ${condition.status}`);
    });
    if (result.projectStatus.status === 'ERROR') {
      process.exit(-500);
    }
  }));


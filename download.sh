#!/usr/bin/env bash
baseUrl=https://static-objects.gitlab.net/shadowy-ng/ci/raw/master/
listDir=(devops)
listFile=(devops/bump.sh devops/sonar-result.js)
echo "---------------- Build folders structure ----------------"
for item in ${listDir[*]}
do
  echo "mkdir ${item}"
  mkdir -p ./${item}
done
echo "---------------- Download files ----------------"
for item in ${listFile[*]}
do
  echo "curl ${baseUrl}${item}"
  curl -sS ${baseUrl}${item} --output ./${item}
  chmod 777 ./${item}
done
